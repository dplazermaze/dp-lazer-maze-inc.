We are the top rated laser tag venue in Toronto, with a spacious 10,000 sq ft. arena and a lobby filled with arcade games. We welcome people of all ages but our speciality is in hosting memorable birthday parties for kids, youth groups, team build, camps, company functions and more.

Address: 31 Gaudaur Rd, Woodbridge, ON L4L 3R8, Canada

Phone: 905-856-1637
